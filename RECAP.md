# Unidad C0: Recapitulación

[Enlace repositorio](https://gitlab.com/luciferma14/bbdd)

Autor: Lucía Ferrandis Martínez

Una base de datos es un sistema que organiza, almacena y gestiona eficientemente grandes cantidades de información. Se utilizan en campos como el comercio electrónico, las finanzas, los recursos humanos y la atención sanitaria. 

Estas bases de datos se realizan mediante SQL, el cual es un lenguaje de consulta estándar que se utiliza para interactuar con bases de datos relacionales como MySQL. Te permite hacer varias cosas con tus datos, como por ejemplo,
seleccionar datos: obtiene información específica de la base de datos; insertar datos: agrega nuevos datos a la base de datos;
editar datos: actualiza la información existente en la base de datos; eliminar datos: elimina datos de la base de datos.


## Concepto y origen de las bases de datos
__¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.__ 

Una base de datos es un sistema que colecciona de forma estructurada los datos almacenados electrónicamente. Permiten el almacenamiento y la gestión eficiente de grandes volúmenes. Los problemas que trata de resolver suelen ser la organización, la seguridad, el acceso y la menera de compartir los datos con otros usuarios. [[1](#referencias)]

## Sistemas de gestión de bases de datos
__¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Deinición de DBMS.__

Un sistema de gestión de bases de datos es un software que permite gestionar,crear y acceder a la bases de datos. También se encarga de organizar, eliminar, modificar. Las características de acceso podria ser el lenguaje de la consulta y también la interfaz gráfica. [[2](#referencias)]

### Ejemplos de sistemas de gestión de bases de datos
__¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?__ 

Los sistemas de gestión de bases de datos más usuados actualmente son [[3](#referencias)] :


* __Oracle DB:__ Ofrece una amplia gama de características, incluyendo seguridad avanzada, gestión de datos de alto rendimiento y disponibilidad continua. Sigue el modelo cliente-servidor.
* __IMB Db2:__ Es un sistema de gestión de bases de datos relacional (RDBMS) de alto rendimiento y escalabilidad. Sigue el modelo cliente-servidor.
* __SQLite:__ Es una base de datos relacional embebida que no requiere un servidor independiente.
* __MariaDB:__ Es una bifurcación de MySQL, por lo que es software libre al igual que MySQL. Sigue el modelo cliente-servidor.
* __SQL Server:__ Es un sistema de gestión de bases de datos relacional (RDBMS) desarrollado por Microsoft, y por lo tanto, libre. Sigue el modelo cliente-servidor.
* __PostgreSQL:__  Es un sistema de gestión de bases de datos de código abierto y libre. Sigue el modelo cliente-servidor.
* __mySQL:__ Es un sistema de gestión de bases de datos de código abierto y libre. Sigue el modelo cliente-servidor.
* __Mongo DB:__ Es un sistema de gestión de bases de datos NoSQL que utiliza documentos JSON para almacenar datos. Sigue el modelo cliente-servidor.
* __Microsoft Access:__ Es una base de datos de escritorio que no requiere un servidor independiente.



## Modelo cliente-servidor [[4](#referencias)] 
__¿Por qué es interesante que el DBMS se encuentre en un servidor?__

Puede tener varias ventajas en un servidor. Como por ejemplo, la seguridad, el rendimiento, la disponibilidad...

__¿Qué ventajas tiene desacoplar al DBMS del cliente?__

Significa que el sistema de gestión de bases de datos (DBMS) no se ejecuta en el mismo dispositivo que el cliente.

__¿En qué se basa el modelo cliente-servidor?__

Es una forma eficiente de distribuir recursos y servicios a un gran número de usuarios.

Definiciones:

* __Cliente__: Máquina (hardware) o software que solicita información al servidor de la base de datos.
* __Servidor__: E5.a', 'Oracle DB'),
('5.b', 'SQL Server'),
('5.c', 'PostgreSQL'),
('5.d', 'Ninguna de las anteriores.'),quipo físico o virtual que almacena la base de datos, ejecuta el DBMS.
* __Red__: Sistema de comunicación que permite la conexión de dos o más dispositivos con el fin de compartir recursos, como datos, hardware y software.
* __Puerto de escucha__: Es un número que identifica un servicio específico en el servidor. El cliente indica el puerto al que desea conectarse en su petición.
* __Petición__: Es un mensaje que el cliente envía al servidor solicitando información o un servicio. 
* __Respuesta__: Es un mensaje que el servidor envía al cliente con la información o el resultado de la operación solicitada.

## SQL
__¿Qué es SQL? ¿Qué tipo de lenguaje es?__

Es un lenguaje de programación que se utiliza para interactuar con bases de datos relacionales.

### Instrucciones de SQL

__DDL:__  Lenguaje de Definición de Datos. Sirve para crear, modificar y eliminar estructuras dentro de la base de datos.

__DML:__ Lenguaje de Manipulación de Datos. Se utiliza para insertar, modificar y eliminar datos dentro de las tablas de la base de datos.

__DCL:__ Lenguaje de Control de Datos. Se utiliza para conceder y revocar permisos a los usuarios de la base de datos.

__TCL:__ Lenguaje de Control de Transacciones. Se utiliza para gestionar transacciones en la base de datos.

## Bases de datos relacionales [[5](#referencias)]
__¿Qué es una base de datos relacional?__ 

Es un tipo de base de datos que organiza los datos en tablas, donde cada fila representa un registro individual y cada columna representa un atributo o característica de ese registro. Las tablas se relacionan entre sí mediante claves, que son campos comunes que permiten conectar la información.

__¿Qué ventajas tiene?__ 

Las ventajas tiene se relacionan con la organización, la eficiencia, la flexibilidad, la seguridad...

__¿Qué elementos la conforman?__

Los elementos que la conforman son las tablas, las columnas, las filas, las claves, las relaciones...

* __Relación (tabla)__: Es la principal estructura para almacenar datos en una base de datos relacional. Se asemeja a una hoja de cálculo con filas y columnas.
* __Atributo/Campo (columna)__: Representa una característica específica de una entidad dentro de una tabla. Aparece como una columna vertical en la tabla.
* __Registro/Tupla (fila)__: Representa un único conjunto de valores para todas las características de una entidad individual. Aparece como una fila horizontal en la tabla.


## Referencias
* [[1] Oracle.com](https://www.oracle.com/es/database/what-is-database/)

* [[2] Intelequia.com](https://intelequia.com/es/blog/post/gestor-de-base-de-datos-qu%C3%A9-es-funcionalidades-y-ejemplos)

* [[3] Wixsite.com](https://salvadortorres77.wixsite.com/asth/single-post/2016/02/04/dbms-libres-y-comerciales)

* [[4] Ionos.es](https://www.ionos.es/digitalguide/servidores/know-how/modelo-cliente-servidor/)

* [[5] Oracle.com](https://www.oracle.com/es/database/what-is-a-relational-database/)
