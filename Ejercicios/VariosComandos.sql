USE empresa;

SELECT *
FROM EMP
WHERE COMISSIO != " " OR COMISSIO >= 0 -- Los que tengan la comisión diferente a null
AND COMISSIO IS NULL -- Los que tengan la comisión null
ORDER BY SALARI DESC -- Comisión más alta a la más baja
LIMIT 2; -- "TOP" 2 (solo saca 2)

SELECT DISTINCT OFICI -- Para que muestre solo una vez cada uno
FROM EMP;

SELECT COUNT(DISTINCT OFICI) -- Cuenta las opciones que hay, sin repetirlas
FROM EMP;

-- ------------------------------ 

USE videoclub;

INSERT INTO ACTOR
VALUES 
	("PACO", 8);

INSERT INTO ACTOR
(Nom, CodiActor) -- Decir como van se estructuran las columnas
VALUES 
	(NULL, 7);
    
INSERT INTO ACTOR
(CodiActor) -- Seleccionar las columnas que estoy rellenando
VALUES 
	(9);
    
DELETE FROM ACTOR -- Borrar contenidos de columnas
WHERE CodiActor = 7;

DELETE FROM ACTOR -- "" sin tener que repetir CodiActor todo el rato
WHERE CodiActor IN (7, 8);
WHERE CodiActor BETWEEN 9 AND 10; Los que están entre esos números
WHERE CodiActor > 11; Los mayores de 11 

UPDATE ACTOR -- Modificar columnas
SET Nom = "JORDI"
WHERE CodiActor = 5;

SELECT 
	P.Titol,
	G.Descripcio
FROM PELICULA P -- A la tabla PELICULA le hago un JOIN con la tabla GENERE
JOIN GENERE G
ON P.CodiGenere = G.CodiGenere;