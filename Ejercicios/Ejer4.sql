SELECT fotos.idFoto,fotos.descripcion, fotos.fechaCreacion, tiposReaccion.descripcion
FROM reaccionesFotos, fotos, tiposReaccion
WHERE reaccionesFotos.idUsuario = 11
    AND reaccionesFotos.idFoto = fotos.idFoto
    AND reaccionesFotos.idTipoReaccion = 4
    AND tiposReaccion.idTipoReaccion = reaccionesFotos.idTipoReaccion;