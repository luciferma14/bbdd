#FOTOS DEL USUARIO 36 TOMADAS EN ENERO DE 2024 
SELECT *
FROM fotos
WHERE fotos.idUsuario = 36
    AND (fotos.fechaCreacion > "2023/12/31/"
    AND fotos.fechaCreacion < "2024/02/01/");
    
#“Otra forma”

SELECT *
FROM fotos
WHERE YEAR(fechaCreacion) = "2024"
    AND MONTH(fechaCreacion) = "1"
    AND idUsuario = 36;