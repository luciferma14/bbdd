SELECT U.nombre, COUNT(*) AS "Número"
FROM roles R
JOIN usuarios U
ON U.idRol = R.idRol
JOIN reaccionesFotos RF
ON U.idUsuario = RF.idUsuario
JOIN tiposReaccion TR
ON TR.idTipoReaccion = RF.idTipoReaccion
WHERE R.descripcion = "Usuario"
AND TR.descripcion = "Me gusta"
GROUP BY U.idUsuario

UNION

SELECT U.nombre, COUNT(*) AS "Número"
FROM usuarios U
JOIN roles R
ON U.idRol = R.idRol
JOIN reaccionesComentarios RC
ON U.idUsuario = RC.idUsuario
JOIN tiposReaccion TR
ON TR.idTipoReaccion = RC.idTipoReaccion
WHERE R.descripcion = "Usuario"
AND TR.descripcion = "Me gusta"
GROUP BY U.idUsuario

-- con subconsultas

SELECT *, 
	(SELECT descripcion 
	FROM roles R 
	WHERE R.idRol = U.idRol) AS Rol 
FROM usuarios U
WHERE U.idRol IN (
	SELECT R.idRol FROM roles R
	WHERE R.descripcion = "Usuario"
    OR R.descripcion = "Administrador"
);

SELECT *
FROM usuarios U, reaccionesFotos
WHERE U.idRol IN (
	SELECT roles.idRol FROM roles
    WHERE roles.descripcion = "Usuario"
    -- OR roles.descripcion = "Administrador"
)
AND idTipoReaccion= (
	SELECT idTipoReaccion FROM tiposReaccion
    WHERE tiposReaccion.descripcion = "Me gusta"
);