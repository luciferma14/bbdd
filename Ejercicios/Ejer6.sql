-- Número de “Me divierte” de la foto número 12 del usuario 45
SELECT F.idFoto, U.idUsuario, U.nombre, COUNT(TP.descripcion) AS "Número de 'Me divierte'"
FROM fotos F, usuarios U, tiposReaccion TP
WHERE F.idFoto = '12'
AND U.idUsuario = '45'
AND TP.descripcion = "Me divierte";